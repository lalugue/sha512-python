sha512-python

An implementation of the SHA-512 algorithm in Python.

Created by:

Angelo Lugue
lalugue@yahoo.com.ph

Darvy Ong
darvy.ong@eee.upd.edu.ph

Kenneth Ung
kenneth.ung@eee.upd.edu.ph
