import binascii  # for binary arrays
# for list to array conversion (weak-typed languages are a pain)
import numpy as np
from bitstring import BitArray
# note: bitstring needs to be installed
import functions as fx
from datetime import datetime
import sys

startTime = datetime.now()

#get the name of the file
filename = sys.argv[1]
print("opening the file: ")
print(filename)

# Settings
np.set_printoptions(threshold=sys.maxsize)


# prepare some initial values
# hash values H - 8 64-bit words
h0 = 0x6a09e667f3bcc908
h1 = 0xbb67ae8584caa73b
h2 = 0x3c6ef372fe94f82b
h3 = 0xa54ff53a5f1d36f1
h4 = 0x510e527fade682d1
h5 = 0x9b05688c2b3e6c1f
h6 = 0x1f83d9abfb41bd6b
h7 = 0x5be0cd19137e2179

#constant K
konst = [0x428a2f98d728ae22, 0x7137449123ef65cd, 0xb5c0fbcfec4d3b2f, 0xe9b5dba58189dbbc, 0x3956c25bf348b538, 0x59f111f1b605d019, 0x923f82a4af194f9b, 0xab1c5ed5da6d8118, 0xd807aa98a3030242, 0x12835b0145706fbe, 0x243185be4ee4b28c, 0x550c7dc3d5ffb4e2, 0x72be5d74f27b896f, 0x80deb1fe3b1696b1, 0x9bdc06a725c71235, 0xc19bf174cf692694, 0xe49b69c19ef14ad2, 0xefbe4786384f25e3, 0x0fc19dc68b8cd5b5, 0x240ca1cc77ac9c65, 0x2de92c6f592b0275, 0x4a7484aa6ea6e483, 0x5cb0a9dcbd41fbd4, 0x76f988da831153b5, 0x983e5152ee66dfab, 0xa831c66d2db43210, 0xb00327c898fb213f, 0xbf597fc7beef0ee4, 0xc6e00bf33da88fc2, 0xd5a79147930aa725, 0x06ca6351e003826f, 0x142929670a0e6e70, 0x27b70a8546d22ffc, 0x2e1b21385c26c926, 0x4d2c6dfc5ac42aed, 0x53380d139d95b3df, 0x650a73548baf63de, 0x766a0abb3c77b2a8, 0x81c2c92e47edaee6, 0x92722c851482353b, 0xa2bfe8a14cf10364, 0xa81a664bbc423001, 0xc24b8b70d0f89791, 0xc76c51a30654be30, 0xd192e819d6ef5218, 0xd69906245565a910, 0xf40e35855771202a, 0x106aa07032bbd1b8, 0x19a4c116b8d2d0c8, 0x1e376c085141ab53, 0x2748774cdf8eeb99, 0x34b0bcb5e19b48a8, 0x391c0cb3c5c95a63, 0x4ed8aa4ae3418acb, 0x5b9cca4f7763e373, 0x682e6ff3d6b2b8a3, 0x748f82ee5defb2fc, 0x78a5636f43172f60, 0x84c87814a1f0ab72, 0x8cc702081a6439ec, 0x90befffa23631e28, 0xa4506cebde82bde9, 0xbef9a3f7b2c67915, 0xc67178f2e372532b, 0xca273eceea26619c, 0xd186b8c721c0c207, 0xeada7dd6cde0eb1e, 0xf57d4f7fee6ed178, 0x06f067aa72176fba, 0x0a637dc5a2c898a6, 0x113f9804bef90dae, 0x1b710b35131c471b, 0x28db77f523047d84, 0x32caab7b40c72493, 0x3c9ebe0a15c9bebc, 0x431d67c49c100d4c, 0x4cc5d4becb3e42b6, 0x597f299cfc657e2a, 0x5fcb6fab3ad6faec, 0x6c44198c4a475817]

shawordmask = 0xFFFFFFFFFFFFFFFF #length of words depending on sha used

# Step 0: get the damn data into a binary thing

f = open(filename, "rb")  # for now, a test file
data = f.read()

# Use BitArray while importing data
bindata = BitArray(data)

# convert the BitArray into a numpy array; operations will be done in numpy
bindata = np.asarray(bindata)
bindata = bindata.astype('int')
#print("bindata: ", bindata)
#print("bindata_size: ",bindata.size)

# Step 1: Preprocess Data

# Add the 1
l = bindata.size  # compute l
#print("size of l is: ", l)
# l + k + 1 = 896 mod 1024
k = 1024 - ((l+1+128) % 1024)
#print("k is :", k)

# prepare the bit arrays
ktail = [0] * k
ltail = [0] * 128
ktail = np.asarray(ktail)
ltail = np.asarray(ltail)

#print("ktail length: ", len(ktail))

# to convert an integer (l) into a array of bits
#larray = np.unpackbits(np.array([l], dtype=np.int64).view(np.uint8))
larray = list(bin(l))
larray = larray[2:]
larray = list(map(int,larray))
#larray = larray[::-1]
#print("larray is: ", larray)
#print("larray_size: ", len(larray))
# set the rightmost part of ltail into larray
#ltail[(ltail.size-larray.size):] |= larray
ltail[len(ltail)-len(larray):] |= larray
#ltail = larray

#print("ltail: ", ltail)
#print(ltail.size-larray.size)  # size is in *bytes*
#print("ltail_size: ", len(ltail))


# create the preprocessed string: binary data ,1, k, and l
preprocess = np.append(bindata, 1)
preprocess = np.append(preprocess, ktail)
preprocess = np.append(preprocess, ltail)

#print("preprocess:", preprocess)
#print("preprocess_size:", preprocess.size)


# Step 2: Process the message in successive 1024-bit chunks
#print("chunking: ")
chunksize = 1024  # probably a var to edit once converted to sha-512
chunks = []

# make chunks
for c in range(0, preprocess.size, chunksize):
    chunks.append(preprocess[c:c+chunksize])

#print("chunks is: ", chunks)
#print("chunks_size: ", len(chunks))

# process each chunks
for c in range(len(chunks)):
    w = []  # 80 entry array of 64 bits per element
    choppedchunk = []

    wordbitlength = 64  # check in sha-512
    #print("in choppedchunk loop: ")
    #for w[0 to 15]
    for d in range(0, chunksize, wordbitlength):  # process from bit 0 to 1023
        choppedchunk.append(fx.numpyToInt64(chunks[c][d:d+wordbitlength]))

    # copy chunk into first 16 words w[0..15] of the message schedule array
    w = choppedchunk

    #print("the first w from choppedchunk is: ")
    #print(int(w[0]))


# Extend the first 16 words into the remaining 64 words w[16..79] of the message schedule array:
    for i in range(16, 80):  # check these hardcoded bounds in sha512

        s0 = fx.numRoll(w[i-15],1) ^ fx.numRoll(w[i-15],8) ^ (w[i-15] >> 7)
        s1 = fx.numRoll(w[i-2],19) ^ fx.numRoll(w[i-2],61) ^ (w[i-2] >> 6)
        wi = (w[i - 16] + s0 + w[i - 7] + s1) & shawordmask
        w.append(wi)

        # Initialize working variables to current hash value:
    a = h0
    b = h1
    c = h2
    d = h3
    e = h4
    f = h5
    g = h6
    h = h7


    # Compression function main loop
    for i in range(0,80):
        S1 = (fx.numRoll(e,14) ^ fx.numRoll(e,18) ^ fx.numRoll(e,41)) & shawordmask
        ch = (e & f) ^ ((~e) & g)
        temp1 = (h + S1 + ch + konst[i] + w[i])

        S0 = (fx.numRoll(a,28) ^ fx.numRoll(a,34) ^ fx.numRoll(a,39)) & shawordmask
        maj = ((a&b)^(a&c)^(b&c))
        temp2 = (S0 + maj)

        h = g
        g = f
        f = e
        e = (d + temp1) & shawordmask
        d = c
        c = b
        b = a
        a = (temp1 + temp2) & shawordmask

    # Add the compressed chunk to the current hash value:
    h0 = (h0 + a) & shawordmask
    h1 = (h1 + b) & shawordmask
    h2 = (h2 + c) & shawordmask
    h3 = (h3 + d) & shawordmask
    h4 = (h4 + e) & shawordmask
    h5 = (h5 + f) & shawordmask
    h6 = (h6 + g) & shawordmask
    h7 = (h7 + h) & shawordmask

digest = format(h0, 'x') + format(h1, 'x') + format(h2, 'x') + format(h3, 'x') + \
    format(h4, 'x') + format(h5, 'x') + format(h6, 'x') + format(h7, 'x')
print("end of hashing result:\n",digest)

print(datetime.now() - startTime)


