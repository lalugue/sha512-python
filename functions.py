#custom functions should probably be added here
import numpy as np

#a shift right function for numpy arrays
def numpyShiftRight(arr,shift):
    temp = np.asarray(arr)
    #print("temp is length: ")
    #print(len(temp))
    rightPart = arr[len(arr)-shift:]
    leftPart = arr[:len(arr)-shift]
    temp[len(temp)-len(leftPart):] = leftPart #this probably was made while brainstorming but not used, sorry bout that

    finalarr = np.zeros(len(rightPart),dtype=np.int)
    #print("finalarr so far: ")
    #print(finalarr)
    #print(len(finalarr))
    finalarr = np.concatenate([finalarr,leftPart])
    return finalarr

#a shift left function for numpy arrays
def numpyShiftLeft(arr,shift):
    temp = np.asarray(arr)
    #print("temp is length: ")
    #print(len(temp))
    rightPart = arr[shift:]
    leftPart = arr[:shift]

    finalarr = np.zeros(len(leftPart),dtype=np.int)
    #print("finalarr so far: ")
    #print(finalarr)
    #print(len(finalarr))
    finalarr = np.concatenate([rightPart,finalarr])
    return finalarr

def numpyToInt32(arr):
    packed = np.packbits(arr)
    #print("packed is: ")
    #print(packed)
    ans = ((packed[3]) + (packed[2] << 8) + (packed[1] << 16) + (packed[0] << 24)) & 0xFFFFFFFF
    if(ans < 0):
        print("alert! wrong ans somewhere in int32 func!")
        print(ans)
        print(packed)
    return ans

def numpyToInt64(arr):
    packed = np.packbits(arr)
    #print("packed is: ")
    #print(packed)
    ans = int((packed[7]) + (packed[6] << 8) + (packed[5] << 16) + (packed[4] << 24) + (packed[3] << 32) + (packed[2] << 40) + (packed[1] << 48) + (packed[0] << 56)) & 0xFFFFFFFFFFFFFFFF
    if(ans < 0):
        print("alert! wrong ans somewhere in int32 func!")
        print(ans)
        print(packed)
    return ans

def numpyToInt(arr):
    #print("arr is: ")
    #print(arr)
    packed = np.packbits(arr).view(np.uint8)
    arrlen = len(packed)
    #print("packed is: ")
    #print(packed)
    ans = 0
    for c in range(arrlen):
        addend = (int(packed[c]) << (8*(arrlen-c-1)) )    & 0xFFFFFFFFFFFFFFFF
        ans = (ans + addend) & 0xFFFFFFFFFFFFFFFF

    print("ans is: ")
    print(ans)
    return ans

def numRoll(num, rollamt):
    #print("num is: ")
    #print(num)
    if num == 0:
        return 0

    #asssumption is a 32-bit number (according to sha256 ROTR specs) ,edit this in other shas
    rightPart = num >> rollamt
    leftPart = num << (64 - rollamt)
    '''
    digitcount = bin(num).count("1") + bin(num).count("0") - 1
    rightPart = num >> (rollamt%digitcount)
    leftPart = num << (digitcount-(rollamt%digitcount))
    '''
    ans = (leftPart | rightPart) & 0xFFFFFFFFFFFFFFFF
    if(ans < 0):
        print("alert! wrong ans somewhere in numroll func!")
        print(ans)
        print(packed)
    #numarr = np.unpackbits(np.array([num], dtype=np.longdouble).view(np.uint8))
    #newarr = np.roll(numarr,rollamt)
    #ans = numpyToInt(newarr)
    return ans
